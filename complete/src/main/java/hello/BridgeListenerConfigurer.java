package hello;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.listener.adapter.ListenerExecutionFailedException;
import org.springframework.stereotype.Component;

import hello.BridgeListenerConfigurer.BridgeDefinition.Direction;

@Component
public class BridgeListenerConfigurer implements JmsListenerConfigurer{

	@Autowired
	@Qualifier(BridgeContants.INTERNAL)
	private JmsTemplate internalJmsTemplate;

	@Autowired
	@Qualifier(BridgeContants.EXTERNAL)
	private JmsTemplate externalJmsTemplate;
	
	private BridgeDefintions definitions = new BridgeDefintions();
	
	@Autowired
	@Qualifier(BridgeContants.INTERNAL)
	private JmsListenerContainerFactory<?>  internalContainer;
	
	@Autowired
	@Qualifier(BridgeContants.EXTERNAL)
	private JmsListenerContainerFactory<?>  externalContainer;
	
	public BridgeListenerConfigurer(){
		definitions.addSimple("marco1-in", Direction.INBOUND);
		definitions.addSimple("marco2-in", Direction.INBOUND);
		definitions.addSimple("marco3-out", Direction.OUTBOUND);
	}
	
	@Override
	public void configureJmsListeners(JmsListenerEndpointRegistrar registrar) {
		
		for(BridgeDefinition definition : definitions) {
			switch (definition.direction) {
			case INBOUND:{
					SimpleJmsListenerEndpoint endpoint = new SimpleJmsListenerEndpoint();
					
					BridgeMessageListener bridgeMessageListener = new BridgeMessageListener(definition);
					endpoint.setConcurrency("1-5");
					endpoint.setDestination(definition.getSource());
					endpoint.setId("bridge-inbound-"+definition.externalDestination);
					endpoint.setMessageListener(bridgeMessageListener);
					bridgeMessageListener.jmsTemplate = externalJmsTemplate;
					registrar.registerEndpoint(endpoint, externalContainer);
				}
				break;
			case OUTBOUND:{
					SimpleJmsListenerEndpoint endpoint = new SimpleJmsListenerEndpoint();
					
					BridgeMessageListener bridgeMessageListener = new BridgeMessageListener(definition);
					endpoint.setConcurrency("1-5");
					endpoint.setDestination(definition.getSource());
					endpoint.setId("bridge-outbound-"+definition.externalDestination);
					endpoint.setMessageListener(bridgeMessageListener);
					bridgeMessageListener.jmsTemplate = internalJmsTemplate;
					registrar.registerEndpoint(endpoint, internalContainer);
				}
				break;
			default:
				throw new IllegalArgumentException("unknown");
			}
			
		}
		
	}
	
	
	public static class BridgeMessageListener implements MessageListener {

		private final BridgeDefinition definition;
		
		private  JmsTemplate jmsTemplate;
		
		private HeaderCopier headerCopier = new HeaderCopier();
		
		public BridgeMessageListener(BridgeDefinition definition) {
			super();
			this.definition = definition;
		}


		@Override
		public void onMessage(Message message) {
			try {
				String body = ((TextMessage) message).getText();
				System.out.println("hello from bridge "+definition.internalDestination+": "+body);
				
				MessageCreator messageCreator = new MessageCreator() {
					
					@Override
					public Message createMessage(Session session) throws JMSException {
						Message copiedMessage = session.createTextMessage(body);
						headerCopier.internalCopy(message, copiedMessage);
						return copiedMessage;
					}
				};
				jmsTemplate.send(definition.getTarget(), messageCreator);
			} catch (JMSException e) {
				throw new ListenerExecutionFailedException("cannot bridge", e);
			}
			
		}
		
	}

	   public static class BridgeDefinition{
	    	
	    	
	    	public BridgeDefinition(String internalDestination, String externalDestination, Direction direction) {
				super();
				this.internalDestination = internalDestination;
				this.externalDestination = externalDestination;
				this.direction = direction;
			}

			private final String internalDestination;
	    	
	    	private final String externalDestination;
	    	
	    	private Direction direction;
	    	
	    	
	    	public String getSource() {
	    		return Direction.INBOUND==direction? externalDestination : internalDestination;
	    	}
	    	
	    	public String getTarget() {
	    		return Direction.INBOUND==direction? internalDestination : externalDestination;
	    	}
	    	
	    	
	    	
	    	
	    	public enum Direction{
	    		INBOUND,
	    		OUTBOUND
	    	}
	    }
	   
	   public static class BridgeDefintions implements Iterable<BridgeDefinition>{
			
			private List<BridgeDefinition> bridgeDefitions = new ArrayList<>();
			
			public void addSimple(String inputDestination, Direction direction) {
				BridgeDefinition bridgeDefinition = new BridgeDefinition(inputDestination, inputDestination+"."+direction, direction);
				bridgeDefitions.add(bridgeDefinition);
			}
			
			public List<BridgeDefinition> getDefitions(){
				return Collections.unmodifiableList(bridgeDefitions);
			}

			@Override
			public Iterator<BridgeDefinition> iterator() {
				return bridgeDefitions.iterator();
			}
			
		}
	   
	public static class HeaderCopier{
		
		
		
		public void internalCopy(Message source, Message target) throws JMSException {
			
			{
				String correlationId = source.getJMSCorrelationID();
				if(correlationId!=null) {
					target.setJMSCorrelationID(correlationId);
				}
			}
			
			if(false){
				//not copied
				Destination destination = source.getJMSDestination();
				if (destination != null) {
					target.setJMSDestination(destination);
				}
				
				//
				target.setJMSRedelivered(source.getJMSRedelivered());
				
				
				Destination jmsReplyTo = source.getJMSReplyTo();
				if(jmsReplyTo!=null) {
					target.setJMSReplyTo(jmsReplyTo);
				}
				
				target.setJMSTimestamp(source.getJMSTimestamp());
			}
			
			{
				target.setJMSDeliveryMode(source.getJMSDeliveryMode());				
			}
			{
				target.setJMSMessageID(source.getJMSMessageID());				
			}
			
			{
				target.setJMSExpiration(source.getJMSExpiration());
			}
			
			{
				target.setJMSPriority(source.getJMSPriority());
			}
			
			{
				target.setJMSType(source.getJMSType());
			}
			
			
			{
				 Enumeration propertyNames = source.getPropertyNames();
				 while(propertyNames.hasMoreElements()) {
					 String nextElement = (String) propertyNames.nextElement();
					 
					 target.setObjectProperty(nextElement, source.getObjectProperty(nextElement));
				 }
			}
			
		}
	}
}
