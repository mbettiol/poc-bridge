
package hello;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.boot.context.properties.PropertyMapper;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

@SpringBootApplication
//@EnableJms
public class Application {

    @Bean
    @Qualifier(BridgeContants.INTERNAL)
    @Primary//for spring boot single...
    public JmsListenerContainerFactory<?> myFactory(@Qualifier(BridgeContants.INTERNAL) ConnectionFactory connectionFactory,
                                                    DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory);
        // You could still override some of Boot's default if necessary.

        return factory;
    }
    
    
    //ActiveMQConnectionFactoryConfiguration
    @Bean(destroyMethod="stop")
    @Qualifier(BridgeContants.INTERNAL)
    @Primary//for spring boot single...
    public PooledConnectionFactory createConnectionFactory() {
    	
    	PooledConnectionFactory pooledConnectionFactory = new org.apache.activemq.pool.PooledConnectionFactory();
    	
    	//can be reused with any connection factory
    	pooledConnectionFactory.setConnectionFactory(new ActiveMQConnectionFactory("tcp://127.0.0.1:61616"));
    	
    	pooledConnectionFactory.setBlockIfSessionPoolIsFull(true);
//    	pooledConnectionFactory.set
    	//maxActiveSessionsPerConnection=500
    	
    	pooledConnectionFactory.setMaxConnections(1);
    	
    	return pooledConnectionFactory;
    }

    
    @Bean
    @Qualifier(BridgeContants.EXTERNAL)
    public JmsListenerContainerFactory<?> externalFactory(@Qualifier(BridgeContants.EXTERNAL) ConnectionFactory connectionFactory,
                                                    DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory);
        // You could still override some of Boot's default if necessary.
        return factory;
    }
    
    
	@Bean
	@Qualifier(BridgeContants.EXTERNAL)
	public JmsTemplate jmsTemplateExternal( @Qualifier(BridgeContants.EXTERNAL) ConnectionFactory connectionFactory) {
//		PropertyMapper map = PropertyMapper.get();
		JmsTemplate template = new JmsTemplate(connectionFactory);
		return template;
	}
	
	@Bean
	@Qualifier(BridgeContants.INTERNAL)
	public JmsTemplate jmsTemplateInternal( @Qualifier(BridgeContants.INTERNAL) ConnectionFactory connectionFactory) {
//		PropertyMapper map = PropertyMapper.get();
		JmsTemplate template = new JmsTemplate(connectionFactory);
		return template;
	}
    
    //ActiveMQConnectionFactoryConfiguration
    @Bean(destroyMethod="stop")
    @Qualifier(BridgeContants.EXTERNAL)
    public PooledConnectionFactory externalConnectionFactory() {
    	
    	PooledConnectionFactory pooledConnectionFactory = new org.apache.activemq.pool.PooledConnectionFactory();
    	
    	//can be reused with any connection factory
    	pooledConnectionFactory.setConnectionFactory(new ActiveMQConnectionFactory("tcp://127.0.0.1:61616"));
    	
    	pooledConnectionFactory.setBlockIfSessionPoolIsFull(true);
//    	pooledConnectionFactory.set
    	//maxActiveSessionsPerConnection=500
    	
    	pooledConnectionFactory.setMaxConnections(1);
    	
    	return pooledConnectionFactory;
    }
    
//    @Bean // Serialize message content to json using TextMessage
//    public MessageConverter jacksonJmsMessageConverter() {
//        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
//        converter.setTargetType(MessageType.TEXT);
//        converter.setTypeIdPropertyName("_type");
//        return converter;
//    }

    public static void main(String[] args) {
        // Launch the application
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);

//        JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);
//
//        // Send a message with a POJO - the template reuse the message converter
//        System.out.println("Sending an email message.");
//        jmsTemplate.convertAndSend("mailbox", new Email("info@example.com", "Hello"));
    }

}
